# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.


class BasicManager(object):

    default_fields = []

    def __init__(self, odooclient, resource_env, default_fields=None):
        self.client = odooclient
        self.resource_env = resource_env
        if default_fields:
            self.fields = default_fields
        else:
            self.fields = self.default_fields

    def _list_or_tuple(self, ids):
        if not isinstance(ids, list) or isinstance(ids, tuple):
            ids = [
                ids,
            ]
        return ids

    def get(self, ids, read=False, fields=None):
        """Get one or more Resources by id.

        Args:
            ids (list/int): can be 1 id, or a list of ids.
            read (bool): read objects back as dict (default is False)
            fields (list/string): a list of field names to include in a read.

        Returns:
            RecordSet: A collection of resources
        """
        query_fields = self.fields
        if fields:
            query_fields = fields

        if read:
            return self.resource_env.read(self._list_or_tuple(ids), fields=query_fields)
        return self.resource_env.browse(self._list_or_tuple(ids))

    def list(self, filters=None, get=True, read=False, fields=None, **kwargs):
        """Get a list of Resources.

        Args:
            filters (list): is a list of search options.
                [('field', '=', value), ]
            get (bool): If true will fetch objects, otherwise just return ids.
            read (bool): read objects back as dict (default is False)
            fields (list/string): a list of field names to include in a read.
            kwargs: direct field comparisons to be uses as filters.
                will be ignored if filters is given.
        
        Returns:
            RecordSet: A collection of resources or ids
        """
        query_fields = self.fields
        if fields:
            query_fields = fields

        if not filters:
            filters = []
        if kwargs:
            filters += [(k, "=", v) for k, v in kwargs.items()]

        ids = self.resource_env.search(filters)
        if get:
            return self.get(ids, read, query_fields)
        else:
            return ids

    def create(self, **fields):
        """Create a Resource.

        Args:
            fields (dict): is the dict of kwargs to pass to create.
                Allows slightly nicer syntax than having to pass in a dict.
        """
        return self.resource_env.create(fields)

    def create_many(self, resources):
        """Create a Resource.

        Args:
            resources (list): is the list of dicts for each item to create.
        """
        return self.resource_env.create(list_of)

    def load(self, fields, rows):
        """Loads in a Resource.

        Args:
            fields (list(str)): is a list of fields to import.
            rows list(list(str)): is the item data.
        """
        return self.resource_env.load(fields=fields, data=rows)

    def delete(self, ids):
        """Delete 1 or more Resources by id.

        Args:
            ids (list(int)): can be 1 id, or a list of ids.

        returns:
            bool: True if deleted or not present.
        """
        return self.resource_env.unlink(self._list_or_tuple(ids))
