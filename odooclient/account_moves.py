# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from odooclient.common import BasicManager


class AccountMoveManager(BasicManager):

    default_fields = [
        'id',
        'name',
        'display_name',
        "attention",
        'type',
        'state',
        'partner_id',
        'invoice_date',
        'invoice_date_due',
        'invoice_payment_term_id',
        'invoice_sent',
        'invoice_user_id',
        'team_id',
        'amount_untaxed',
        'amount_tax',
        'amount_total',
        'amount_residual',
        'invoice_payment_state',
        'reference_type',
        'ref',
        'os_project',
    ]

    def recalculate(self, order_ids):
        order_ids = self._list_or_tuple(order_ids)
        self.resource_env.button_compute(order_ids, set_total=True)

    def validate(self, order_ids):
        order_ids = self._list_or_tuple(order_ids)
        self.resource_env.signal_workflow(order_ids, 'invoice_open')

    def send(self, order_ids, recipients=None, template_id=None, context=None):
        """Send invoice.

        In odoo the context values can be used in the email template as:
        ${ctx.get('some_test_key')}
        or
        ${ctx.some_test_key}
        """
        context = context or {}
        email_context = {}

        if recipients:
            context['partner_ids'] = recipients
        if template_id:
            email_context['template_id'] = template_id
            email_context['default_template_id'] = template_id

        self.resource_env.force_invoice_send(
            self._list_or_tuple(order_ids),
            context=context,
            email_ctx_override=email_context)
