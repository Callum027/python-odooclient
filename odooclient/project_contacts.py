# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from datetime import timedelta, datetime
from collections import defaultdict
import logging

from .common import BasicManager

LOG = logging.getLogger(__name__)


contact_types_cache = None
last_cache_update = None

blacklisted_contact_types = [
    "owner",
    "primary",
    "reseller customer",
]


class OpenStackProjectContactManager(BasicManager):

    def find_project_contacts(self, project_id, types):
        project = self.client.projects.get_by_os_id(project_id)

        if not project:
            return {}

        search = [
            ('project', '=', project['id']),
            ('contact_type', 'in', list(types)),
        ]
        project_contacts = self.client.project_contacts.list(
            search, read=True)

        type_partner_mapping = defaultdict(list)
        for contact in project_contacts:
            type_partner_mapping[contact['contact_type']].append(
                contact['partner'][0])

        contacts_by_type = defaultdict(list)
        for c_type, partner_ids in type_partner_mapping.items():
            search = [('id', 'in', list(partner_ids)), ]
            partners = self.client.partners.list(search, read=True)
            for partner in partners:
                contacts_by_type[c_type].append(partner)

        return contacts_by_type

    def _get_contact_types(self):
        global contact_types_cache
        global last_cache_update

        now = datetime.utcnow()
        expiry_date = now - timedelta(hours=24)

        if (not contact_types_cache
                or not last_cache_update
                or last_cache_update < expiry_date):
            last_cache_update = now
            contact_types_cache = [
                tag[0] for tag in
                self.resource_env._columns['contact_type'].selection]

        return list(contact_types_cache)

    def get_editable_contact_types(self):
        contact_types = self._get_contact_types()

        for blacklisted_contact_type in blacklisted_contact_types:
            if blacklisted_contact_type in contact_types:
                contact_types.remove(blacklisted_contact_type)
        return contact_types
