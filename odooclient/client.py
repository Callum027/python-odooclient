# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import odoorpc

from odooclient.common import BasicManager

from odooclient.account_moves import AccountMoveManager
from odooclient.partners import PartnerManager

from odooclient.products import ProductManager
from odooclient.projects import OpenStackProjectManager
from odooclient.project_contacts import OpenStackProjectContactManager
from odooclient.volume_discount_ranges import OpenStackVolumeDiscountRangeManager


class Client(object):
    """docstring for OdooClient"""

    def __init__(self, **config):

        self._odoo = odoorpc.ODOO(
            config.get("hostname"),
            protocol=config.get("protocol"),
            port=int(config.get("port")),
            version=config.get("version"),
        )

        self._odoo.login(
            config.get("database"), config.get("user"), config.get("password")
        )

        self.USER_ID = self._odoo.env.uid
        print(self._odoo.env)

        # Core Odoo managers:
        self.sale_orders = BasicManager(
            self,
            self._odoo.env["sale.order"],
            [
                "id",
                "partner_id",
                "name",
                "display_name",
                "state",
                "note",
                "amount_untaxed",
                "date_order",
                "client_order_ref",
                "invoice_status",
            ],
        )
        self.sale_order_lines = BasicManager(
            self,
            self._odoo.env["sale.order.line"],
            # [
            #     "id",
            #     "partner_id",
            #     "name",
            #     "display_name",
            #     "state",
            #     "note",
            #     "amount_untaxed",
            #     "date_order",
            #     "client_order_ref",
            # ],
        )
        self.account_moves = AccountMoveManager(self, self._odoo.env["account.move"])
        self.account_move_lines = BasicManager(
            self, self._odoo.env["account.move.line"]
        )
        self.partners = PartnerManager(self, self._odoo.env["res.partner"])
        self.price_lists = BasicManager(self, self._odoo.env["product.pricelist"])
        self.products = ProductManager(
            self,
            self._odoo.env["product.product"],
            [
                "id",
                "name",
                "uom_id",
                "list_price",
                # "taxes_id",
                # "property_account_income_id",
            ],
        )
        self.countries = BasicManager(
            self, self._odoo.env["res.country"], ["id", "name", "code"]
        )
        self.mail_messages = BasicManager(self, self._odoo.env["mail.message"])
        self.sales_teams = BasicManager(self, self._odoo.env["crm.team"])

        # OpenStack Odoo managers
        self.projects = OpenStackProjectManager(
            self, self._odoo.env["openstack.project"]
        )
        self.project_contacts = OpenStackProjectContactManager(
            self, self._odoo.env["openstack.project_contact"]
        )
        self.credits = BasicManager(self, self._odoo.env["openstack.credit"])
        self.credit_transactions = BasicManager(self, self._odoo.env["openstack.credit.transaction"])
        self.credit_types = BasicManager(self, self._odoo.env["openstack.credit.type"])
        self.customer_groups = BasicManager(
            self, self._odoo.env["openstack.customer_group"]
        )
        self.grants = BasicManager(self, self._odoo.env["openstack.grant"])
        self.grant_types = BasicManager(self, self._odoo.env["openstack.grant.type"])
        self.referrals = BasicManager(self, self._odoo.env["openstack.referral_code"])
        self.resellers = BasicManager(self, self._odoo.env["openstack.reseller"])
        self.reseller_tiers = BasicManager(self, self._odoo.env["openstack.reseller.tier"])
        self.support_subscriptions = BasicManager(
            self, self._odoo.env["openstack.support_subscription"]
        )
        self.term_discounts = BasicManager(
            self, self._odoo.env["openstack.term_discount"]
        )
        self.trials = BasicManager(self, self._odoo.env["openstack.trial"])
        self.volume_discount_ranges = OpenStackVolumeDiscountRangeManager(
            self, self._odoo.env["openstack.volume_discount_range"]
        )
        self.voucher_codes = BasicManager(
            self, self._odoo.env["openstack.voucher_code"]
        )
